#include "StackLL.h"
#include <iostream>
#include <stdexcept>
using namespace std;

class Stack::Node //self-referential Node class
{
	public:
	   int data = 0;
	   Node* link = nullptr;
	   //link is a data member which is a pointer 
	   //to an object of the same type (i.e. Node)
	
	};//end Node class definition (can only be seen by the List class)

int Stack::size(){
	return num_elements;
	}
	
int Stack::top(){				
	if(num_elements == 0){
		throw out_of_range("Stack::top Failed. Stack empty");//throw an "out_of_range" exception
		}
	else {
		

	Node* tmpPtr = frontPtr;
	
	if(num_elements == 1){
	   return (tmpPtr->data);  
	 }
	else{  
	
	  Node* tempPtr = frontPtr;
	  int loc = 1; 
	  
	    while( loc != num_elements) //get pointer to (k-1)th node
	     {
		tempPtr = tempPtr->link;
		loc++;
	     }
		
	    return tempPtr->data; 
        }//end else
	 
 
 }
	return 0;
		
}
	
	
void Stack::push(int val)
{
	
	Node* newPtr = new Node{val};
	
	if(num_elements == 0)
	{
	  newPtr->link = frontPtr;
	  frontPtr = newPtr;
	 }
	else
	 {  
	
	  Node* tmpPtr = frontPtr;
	  int loc = 1; 
	  
	    while( loc != num_elements) //get pointer to (k-1)th node
	     {
		tmpPtr = tmpPtr->link;
		loc++;
	     }
	
	  newPtr->link = tmpPtr->link;
	  tmpPtr->link = newPtr;  
        }//end else

     num_elements++;
 }
 
 void Stack::pop()
{
	
	if (num_elements == 0)//if the location is invalid 
	     throw out_of_range("Stack::pop failed. Stack empty");//throw an "out_of_range" exception
	
	
	Node* delPtr;
	
	if(num_elements == 1)
	{
	  delPtr = frontPtr;
	  frontPtr = frontPtr->link;
	 }
	 else
	 {
	    Node* tmpPtr = frontPtr;
		
	    int loc = 1;
            
            while(loc != num_elements -1)//get pointer to (k-1)th node
	    {
	       tmpPtr = tmpPtr->link;
		loc++;
	    }
	
	    delPtr = tmpPtr->link;
	    tmpPtr->link = delPtr->link;
	  }
	
	delete delPtr;
	num_elements--;
	}
	
void Stack::clear(){
	 
	while(num_elements > 0)
      pop();
	
}

Stack::~Stack()
{
    while(num_elements > 0)
      pop();
}
