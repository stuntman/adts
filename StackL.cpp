#include "StackL.h"


int Stack::size() {
	return data.size();
}

void Stack::push(int val){
	data.insert(val, data.size() + 1);
}

int Stack::top(){
	return data.get(data.size());
	}
	
void Stack::pop(){
	data.remove(data.size());
	
	}
void Stack::clear(){
	data.clear();
	}
	
	

