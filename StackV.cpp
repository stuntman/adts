#include "StackV.h"

int Stack::size(){
	return data.size();

	}
	
int Stack::top(){
	if(data.back()>-1)
	return data.back();

	
	}
	
void Stack::clear(){
    while (data.size() > 0){
	data.pop_back();
	}
	}
	
void Stack::push(int val){
	data.push_back(val);

	}
	
void Stack::pop(){
	
	data.pop_back();
	}
