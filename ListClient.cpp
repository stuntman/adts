#include <iostream>
#include "List.h"

using namespace std;

int main()
{

 List L1, L2; //Declare two list objects, L1 and L2


 cout << "Welcome to my List ADT client"<<endl<<endl;

 //Do some stuff with L1, L2, ... (Eg. cout<<L1.size(); )
 // ...
 
 cout <<"The size of object L1 is "<< L1.size() << endl;
 cout <<"The size of object L2 is "<< L2.size() << endl << endl;
 
 L1.insert(2, 1);
 L1.insert(4, 2);
 L1.insert(55, 3);
 L1.insert(99, 4);
 L2.insert(8, 1);
 L2.insert(45, 2);
 L2.insert(78, 3);
 
 
 
 cout <<"The value at List 1 node 1 is " << L1.get(1) << endl;
 cout <<"The value at List 1 node 2 is " << L1.get(2) << endl;

 cout <<"The value at List 2 node 1 is " << L2.get(1) << endl;
 cout <<"The value at List 2 node 2 is " << L2.get(2) << endl;
 cout << endl;
 
 
 cout <<"The size of object L1 is "<< L1.size()<< endl;
 cout <<"The size of object L2 is "<< L2.size()<< endl<<endl;;
 
 L1.clear();
 
 cout <<"The size of object L1 is "<< L1.size()<< endl;
 cout <<"The size of object L2 is "<< L2.size()<< endl;
 
}

